metaMASTOR is a meta-analysis association test for association of genetic variant (SNP) with (quantitative) phenotype. It allows multiple studies to use the retrospective MASTOR model, which allows metaMASTOR to take advantage of incomplete data. metaMASTOR only needs summary statistics, which can be generated with MASTOR, and no individual level data.

### What is this repository for? ###

* This is a developmental repository, which always has the most up-to-date version of metaMASTOR.

### References ###

* The paper describing MASTOR can be found at
http://www.cell.com/ajhg/pdf/S0002-9297(13)00122-5.pdf
* Technical report describing metaMASTOR can be found at
https://notendur.hi.is/johannaj/metaMASTOR.pdf

### Funding ###
Funding for the development of metaMASTOR was provided by

* The Icelandic Heart Association.
* The Icelandic Research Fund (130726-051, 130726-052, 130726-053 to Johanna Jakobsdottir).

### License ###
The metaMASTOR program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.